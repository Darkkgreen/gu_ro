## Championship Back End
---
Projeto que controla e simula um campeonato, gerando todos os times e o sistema do campeonato, com todas as fases.

O projeto está hospedado em: http://gu-ro.herokuapp.com

### Desenho do Banco de Dados

![picture](https://bitbucket.org/Darkkgreen/gu_ro/raw/master/app/assets/images/database_draw.png)

### Melhorias:
---

1. Adicionar autenticação via devise em conjunto com o Active_admin
2. Corrigir algumas operações para que o CRUD funcione 100% dentro do Active_admin
3. Adicionar jobs, os quais trabalhem ao background para garantir o funcionamento correto da plataforma

### Dependencias:
---

- PostgreSQL 9.6.1
- Ruby >= 2.4.1

Caso você não possua o PostgreSQL instalado na sua máquina, você pode utilizar o comando:
```
docker run \            
    --name postgresql \
    -p 5432:5432 \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=postgres \
    -d \
    postgres:9.6.1 \
```
O Docker cuidará de subir uma instância para que você possa utilizar.
Para instalar o Ruby, você pode realizar a instalação do RVM, um manager de versões do Ruby, o qual utilizarei abaixo.

### Rails usage:
---

1. Clone este repositório (`git clone https://bitbucket.org/Darkkgreen/gu_ro/src/master/`)
2. Para utilizar os comandos rails, é necessário que você instale algumas dependências

```sh
rvm install 2.4.1
gem install bundler
```

3. Dentro do repositório, utilize `bundle install` para instalar as `gems`
4. Com todas as gemas instaladas, execute `bundle exec rake db:create db:migrate` para preparar o banco de dados
5. Após preparar o banco de dados, utilize `rails s` para subir o servidor
6. Acesse a aplicação em `localhost:3000`

### Funcionalidades:
---

![gif](https://bitbucket.org/Darkkgreen/gu_ro/raw/master/app/assets/images/usage.gif)

Utilizar a interface do active_admin, é possível que você:

##### Campeonato
1. Gere e delete Campeonatos

##### Grupos
1. Visualize os grupos da fase de grupos que foram gerados
2. As tabelas dos grupos

##### Jogos
1. Todos os jogos gerados
2. Em quais fases esses jogos aconteceram

##### Times
1. As pontuações e saldos dos times
2. Os jogos que aquele time jogou
3. Seus oponentes

### Testing:

Para executar os testes, é necessário executar o comando `bundle exec rspec`.
Você poderá executar testes separadamente indicando o local do arquivo. (`bundle exec rspec spec/<file location>`)

### Creator:
- [@Darkkgreen](https://github.com/Darkkgreen)