module GameHelper
  def coin_flip
    rand(2).equal?(0)
  end

  def loser_result
    rand(15)
  end

  def winner
    result_team_one.equal?(16) ? team_one : team_two
  end

  def loser
    result_team_one.equal?(16) ? team_two : team_one
  end

  def round_balance
    if result_team_one.equal?(16)
      result_team_one - result_team_two
    else
      result_team_two - result_team_one
    end
  end

  def phase
    group_id.empty? ? 'Playoffs' : 'Groups'
  end
end
