module ChampionshipHelper
  # Group Phase
  def generate_group_phasing
    create_teams
    create_groups
    start_groups
  end

  def simulate_group_phasing
    simulate_groups
    define_winners
  end

  # Playoffs
  def generate_playoffs
    while Team.winners(id).size > 1
      increase_playoffs_level
      create_matches
      generate_playoff_results
    end
  end

  # Generating values and calling helping functions
  def create_teams
    80.times do
      Team.create!(name: 'Team ' + Faker::Name.unique.name_with_middle,
                   group_id: nil,
                   championship_id: id)
    end
  end

  def create_groups
    16.times do |count|
      Group.create!(championship_id: id, number: (count + 1))
    end
  end

  def start_groups
    groups.each(&:start_group)
  end

  def simulate_groups
    groups.each(&:simulate_group)
  end

  def define_winners
    groups.each(&:playoff_teams)
  end

  def create_matches
    while Team.winners(id).size.positive?
      team_one = Team.winners(id).first
      team_one.winner_toggle

      team_two = Team.winners(id).first
      team_two.winner_toggle

      Game.create(team_one_id: team_one.id,
                  team_two_id: team_two.id,
                  championship_id: id,
                  playoff_level: playoff_level)
    end
  end

  def generate_playoff_results
    Game.where(playoff_level: playoff_level).each do |game|
      game.generate_result(false)
    end
  end

  def phase_name(phase)
    names = ['Grupos',
             'Décima-sextas de final',
             'Oitavas de final',
             'Quartas de final',
             'Semifinais',
             'Final']

    names[phase]
  end

  def winner
    return nil unless playoff_level.equal?(5)
    games.where(playoff_level: 5).first.winner
  end
end
