module GroupHelper
  def playoff_teams
    team_group_tables.order('points desc, balance desc').limit(2).each do |t|
      t.team.winner_toggle
    end
  end

  def generate_results
    games.each(&:generate_result)
  end

  def select_random_teams
    Team.random(championship).limit(championship.group_size).each do |team|
      team.create_team_table(id)
    end
  end

  def generate_matches
    possible_matches = teams.ids.map(&:to_s).combination(2).to_a
    possible_matches.each do |teams|
      Game.create(team_one_id: teams[0],
                  team_two_id: teams[1],
                  group_id: id,
                  championship_id: championship.id)
    end
  end
end
