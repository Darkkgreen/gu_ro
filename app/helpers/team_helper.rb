module TeamHelper
  def team_points
    team_group_table.points
  end

  def team_balance
    team_group_table.balance
  end

  def games
    team_one_games.or(team_two_games)
  end

  def winner_toggle
    ActiveRecord::Base.transaction do
      if won_last_phase
        update!(won_last_phase: false)
      else
        update!(won_last_phase: true)
      end
    end
  end

  def create_table
    TeamGroupTable.create(team_id: id, group_id: group_id)
  end
end
