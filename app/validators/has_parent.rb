class HasParent < ActiveModel::Validator
  def validate(record)
    return if parent?(record)

    record.errors[:game] << 'Um game deve possuir relação com algum campeonato ou grupo'
  end

  private

  def parent?(record)
    return true unless record.group.nil? && record.championship.nil?
    return false if !record.championship.nil? && record.playoff_level.zero?
    true
  end
end
