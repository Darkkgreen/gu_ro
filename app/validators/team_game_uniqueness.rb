class TeamGameUniqueness < ActiveModel::Validator
  def validate(record)
    return if unique_in_game?(record)

    record.errors[:game] << 'Um time deve ser único dentro de um game'
  end

  private

  def unique_in_game?(record)
    return true if record.team_one.nil? || record.team_two.nil?
    return true if record.team_one.id != record.team_two.id
  end
end
