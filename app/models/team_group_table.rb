class TeamGroupTable < ApplicationRecord
  validates :team, :group, presence: true
  validates :team, uniqueness: true

  belongs_to :team
  belongs_to :group

  def update_info(balance)
    self.points += 1
    self.balance += balance
    update_table
  end

  private

  def update_table
    ActiveRecord::Base.transaction do
      update!(points: self.points, balance: self.balance)
    end
  end
end
