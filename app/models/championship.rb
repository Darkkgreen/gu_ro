class Championship < ApplicationRecord
  include ChampionshipHelper

  has_many :groups, dependent: :destroy
  has_many :teams, dependent: :destroy
  has_many :games, dependent: :destroy
  validates :group_size, numericality: { greater_than: 1 }

  def generate
    generate_group_phasing
    simulate_group_phasing
    generate_playoffs
  end

  private

  def increase_playoffs_level
    ActiveRecord::Base.transaction do
      update!(playoff_level: playoff_level + 1)
    end
  end
end
