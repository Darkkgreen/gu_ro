class Team < ApplicationRecord
  include TeamHelper

  validates :name, presence: true, uniqueness: true, on: :create
  validates :championship, presence: true

  belongs_to :group, optional: true
  belongs_to :championship

  has_one :team_group_table, dependent: :destroy
  has_many :team_one_games,
           class_name: 'Game',
           foreign_key: 'team_one_id',
           dependent: :nullify
  has_many :team_two_games,
           class_name: 'Game',
           foreign_key: 'team_two_id',
           dependent: :nullify

  # Brings a subset of teams without group, randomized
  scope :random, ->(champ) {
                   where(group_id: nil, championship_id: champ)
                     .order(Arel::Nodes::NamedFunction.new('RANDOM', []))
                 }

  # Brings all the winners from the last phase, scrambled
  scope :winners, ->(champ) {
                    where(won_last_phase: true, championship_id: champ)
                      .order(Arel::Nodes::NamedFunction.new('RANDOM', []))
                  }

  def create_team_table(group_id)
    update_group(group_id)
    create_table
  end

  private

  def update_group(id)
    ActiveRecord::Base.transaction do
      update!(group_id: id)
    end
  end
end
