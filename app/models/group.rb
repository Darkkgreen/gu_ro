class Group < ApplicationRecord
  include GroupHelper
  belongs_to :championship

  has_many :teams, dependent: :nullify
  has_many :games
  has_many :team_group_tables

  def start_group
    select_random_teams
    generate_matches
  end

  def simulate_group
    generate_results
  end
end
