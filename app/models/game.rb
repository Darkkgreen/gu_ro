class Game < ApplicationRecord
  include ActiveModel::Validations
  include GameHelper

  belongs_to :team_one, class_name: 'Team'
  belongs_to :team_two, class_name: 'Team'
  belongs_to :group, optional: true
  belongs_to :championship, optional: true

  validates :team_one, :team_two, presence: true
  validates :result_team_one,
            :result_team_two,
            numericality: { only_integer: true,
                            greater_than_or_equal_to: 0,
                            less_than_or_equal_to: 16 }
  validates_with TeamGameUniqueness
  validates_with HasParent

  def generate_result(group = true)
    update_result
    update_info_result(group)
  end

  private

  def update_result
    ActiveRecord::Base.transaction do
      if coin_flip
        update!(result_team_one: 16, result_team_two: loser_result)
      else
        update!(result_team_one: loser_result, result_team_two: 16)
      end
    end
  end

  def update_info_result(group)
    if group
      winner.team_group_table.update_info(round_balance)
    else
      winner.winner_toggle
    end
  end
end
