ActiveAdmin.register Group do
  belongs_to :championship

  actions :all, except: %i[new edit destroy]

  config.filters = false
  config.sort_order = 'number_asc'

  controller do
    def show
      begin
        super
      rescue ActiveRecord::RecordNotFound => exception
        flash[:error] = 'Grupo não encontrado'

        return redirect_to(root_path) if @championship.nil?
        redirect_to championship_groups_path(@championship.id)
      end
    end
  end

  index do
    column :number do |value|
      'Grupo ' + value.number.to_s
    end

    column 'Quantidade de times' do |value|
      value.teams.count
    end

    column 'Quantidade de jogos' do |value|
      value.games.count
    end

    column 'Primeiro lugar' do |value|
      value.team_group_tables
           .order('points desc, balance desc')
           .limit(2)
           .first.team
    end

    column 'Segundo lugar' do |value|
      value.team_group_tables
           .order('points desc, balance desc')
           .limit(2)
           .last.team
    end

    actions
  end

  show do
    attributes_table do
      row 'Nome' do
        group.number
      end

      row 'Campeonato' do
        group.championship
      end
    end

    panel "Tabela - grupo #{group.number}" do
      table_for group.team_group_tables.order('points desc, balance desc') do
        column 'ID do time', :team_id
        column 'Nome do time', &:team
        column 'Pontos', :points
        column 'Saldo', :balance
      end
    end
  end
end
