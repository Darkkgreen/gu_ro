ActiveAdmin.register Team do
  belongs_to :championship
  actions :all, except: %i[new edit destroy]

  filter :name, as: :select
  filter :group, collection: -> { Group.where(championship_id: params[:championship_id]).map(&:id) }

  controller do
    def show
      begin
        super
      rescue ActiveRecord::RecordNotFound => exception
        flash[:error] = 'Time não encontrado'

        return redirect_to(root_path) if @championship.nil?
        redirect_to championship_teams_path(@championship.id)
      end
    end
  end

  index do
    column :id

    column :name

    column :group

    column :team_points

    column :team_balance

    actions
  end

  show do
    attributes_table do
      row 'Nome' do
        team.name
      end

      row 'Venceu última partida?' do
        team.won_last_phase? ? 'Sim' : 'Não'
      end

      row 'Grupo' do
        team.group
      end

      row 'Campeonato' do
        team.championship
      end

      row 'Tabela na fase de grupos' do
        '#' + team.team_group_table.id.to_s
      end

      row 'Posição (Fase de Grupos)' do
        position = %w[Primeiro Segundo Terceiro Quarto Quinto]
        position[team.group
                 .team_group_tables
                 .order('points desc, balance desc')
                 .pluck(:team_id)
                     .index(team.id)]
      end

      row 'Pontuação (Fase de Grupos)' do
        team.team_points
      end

      row 'Saldo de rounds (Fase de Grupos)' do
        team.team_balance
      end
    end

    team.games.order('playoff_level desc, id desc').each do |game|
      panel team.championship.phase_name(game.playoff_level) do
        table_for game do
          column 'ID da partida', :id
          column 'Time 1', &:team_one
          column 'Resultado time 1', :result_team_one
          column 'Resultado time 2', :result_team_two
          column 'Time 2', &:team_two
        end
      end
    end
  end
end
