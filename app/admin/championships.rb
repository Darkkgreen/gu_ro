ActiveAdmin.register Championship do
  menu priority: 1, label: 'Campeonatos'
  permit_params :name

  actions :all
  filter :name_contains, label: 'Nome contém'

  action_item :view, only: :show do
    link_to 'Gerar', generate_championship_path(championship.id), method: :put, class: 'member_link' unless championship.playoff_level.equal?(5)
  end

  member_action :generate, method: :put do
    championship = Championship.find(params[:id])
    championship.generate
    redirect_to root_path
  end

  controller do
    def show
      begin
        super
      rescue ActiveRecord::RecordNotFound => exception
        flash[:error] = 'Campeonato não encontrado'
        redirect_to root_path
      end
    end
  end

  index do
    column :id

    column 'Nome', :name

    column 'Times por Grupo', :group_size

    column 'Times inscritos' do |championship|
      championship.teams.count
    end

    column 'Finalizado?' do |championship|
      championship.playoff_level.equal?(5) ? 'Sim' : 'Não'
    end

    column 'Vencedor' do |championship|
      championship.winner || '-'
    end

    actions do |championship|
      item 'Gerar', generate_championship_path(championship.id), method: :put, class: 'member_link' unless championship.playoff_level.equal?(5)
      item 'Grupos', championship_groups_path(championship.id), class: 'member_link' unless championship.groups.size.zero?
      item 'Times', championship_teams_path(championship.id), class: 'member_link' unless championship.teams.size.zero?
      item 'Jogos', championship_games_path(championship.id), class: 'member_link' unless championship.games.size.zero?
    end
  end

  form do |f|
    f.inputs 'Detalhes do Campeonato' do
      f.input :name
    end
    f.actions
  end

  show do
    attributes_table do
      row 'Nome' do
        championship.name
      end

      row 'Tamanho dos grupos' do
        championship.group_size
      end

      row 'Playoffs' do
        championship.playoff_level.to_s + ' níveis'
      end

      row 'Campeão' do
        championship.winner
      end
    end
  end
end
