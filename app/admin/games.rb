ActiveAdmin.register Game do
  belongs_to :championship
  permit_params :result_team_one, :result_team_two

  actions :all, except: %i[new destroy]

  config.filters = false

  controller do
    def show
      begin
        super
      rescue ActiveRecord::RecordNotFound => exception
        flash[:error] = 'Game não encontrado'

        return redirect_to(root_path) if @championship.nil?
        redirect_to championship_games_path(@championship.id)
      end
    end
  end

  index do
    column 'Grupo' do |game|
      game.group || '-'
    end

    column 'Fase' do |game|
      championship.phase_name(game.playoff_level)
    end

    column 'Time um', :team_one

    column 'Time dois', :team_two

    column 'Resultado' do |game|
      game.result_team_one.to_s + ' x ' + game.result_team_two.to_s
    end

    column 'Time vencedor', &:winner

    column 'Saldo', &:round_balance

    actions
  end
end
