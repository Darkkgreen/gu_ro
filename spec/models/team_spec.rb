require 'rails_helper'

RSpec.describe Team, type: :model do
  context 'validations' do
    it { should validate_uniqueness_of(:name) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:championship) }

    it 'should raise an error when using the same name' do
      champ = Championship.create(name: 'Champ')
      group = Group.create(number: 1, championship_id: champ.id)

      expect do
        Team.create!(name: 'test',
                     group_id: group.id,
                     championship_id: champ.id)
      end.to_not raise_error(ActiveRecord::RecordInvalid)

      expect do
        Team.create!(name: 'test',
                     group_id: group.id,
                     championship_id: champ.id)
      end.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'functions' do
    it 'should create a table when calling the function' do
      champ = Championship.create(name: 'Champ')
      group = Group.create(number: 1, championship_id: champ.id)
      team = Team.create!(name: 'test',
                          championship_id: champ.id)

      expect do
        team.create_team_table(group.id)
      end.to_not raise_error(ActiveRecord::RecordInvalid)

      expect(TeamGroupTable.first.team.id).to eql(team.id)
    end
  end
end
