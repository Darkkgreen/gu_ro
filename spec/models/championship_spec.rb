require 'rails_helper'

RSpec.describe Championship, type: :model do
  context 'dependent' do
    it 'should destroy everything when deleted' do
      champ = Championship.create(name: 'Champ')
      group = Group.create(number: 1, championship_id: champ.id)
      team_one = Team.create!(name: Faker::Esport.unique.team,
                              group_id: group.id,
                              championship_id: champ.id)
      team_two = Team.create!(name: Faker::Esport.unique.team,
                              group_id: group.id,
                              championship_id: champ.id)
      Game.create!(team_one_id: team_one.id,
                   team_two_id: team_two.id,
                   group_id: group.id,
                   championship_id: champ.id)

      expect do
        champ.destroy
      end.to_not raise_error

      expect(Group.all.size).to eql(0)
      expect(Team.all.size).to eql(0)
      expect(Game.all.size).to eql(0)
      expect(Championship.all.size).to eql(0)
    end
  end

  context 'function' do
    it 'should have a winner by the end of execution' do
      champ = Championship.create(name: 'Champ')

      expect do
        champ.generate
      end.to_not raise_error

      expect(champ.winner).to_not be_nil
    end
  end
end
