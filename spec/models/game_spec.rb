require 'rails_helper'

RSpec.describe Game, type: :model do
  context 'validations' do
    it { should validate_presence_of(:team_one) }
    it { should validate_presence_of(:team_two) }

    it 'should raise an error when using the same team twice on a game' do
      champ = Championship.create(name: 'Champ')
      group = Group.create(number: 1, championship_id: champ.id)
      team_one = Team.create!(name: Faker::Esport.unique.team,
                              group_id: group.id,
                              championship_id: champ.id)
      team_two = Team.create!(name: Faker::Esport.unique.team,
                              group_id: group.id,
                              championship_id: champ.id)

      expect do
        Game.create!(team_one: team_one.id,
                     team_two: team_two.id,
                     result: [16, 5])
      end.to_not raise_error(ActiveRecord::RecordInvalid)

      expect do
        Game.create!(team_one: team_one.id,
                     team_two: team_one.id,
                     result: [16, 5])
      end.to_not raise_error(ActiveRecord::RecordInvalid)
    end

    it 'should raise an error when you do not inform teams ids' do
      expect do
        Game.create!(team_one: nil, team_two: nil, result: [0, 0])
      end.to raise_error
    end
  end
end
