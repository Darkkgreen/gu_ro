require 'rails_helper'

RSpec.describe Group, type: :model do
  context 'functions' do
    it 'should generate the matches' do
      champ = Championship.create(name: 'Champ')
      group = Group.create(number: 1, championship_id: champ.id)
      5.times do
        Team.create!(name: Faker::Name.unique.name, championship_id: champ.id)
      end

      group.select_random_teams

      expect(group.teams.size).to eql(5)

      expect do
        group.generate_matches
      end.to_not raise_error(ActiveRecord::RecordInvalid)

      expect(TeamGroupTable.where(group_id: group.id)
                           .size).to eql(group.teams.size)

      expect(group.games.size).to eql(10)
    end

    it 'should generate the results' do
      champ = Championship.create(name: 'Champ')
      group = Group.create(number: 1, championship_id: champ.id)
      5.times do
        Team.create!(name: Faker::Name.unique.name, championship_id: champ.id)
      end

      group.start_group

      expect do
        group.simulate_group
      end.to_not raise_error(ActiveRecord::RecordInvalid)

      expect(group.games
                  .inject(0) { |sum, e| sum += e.round_balance }).to_not eql(0)
    end
  end
end
