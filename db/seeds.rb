# frozen_string_literal: true

require Rails.root.join('db', 'seed_data', 'championship.rb')
require Rails.root.join('db', 'seed_data', 'teams.rb')
require Rails.root.join('db', 'seed_data', 'groups.rb')
require Rails.root.join('db', 'seed_data', 'group_phase.rb')
require Rails.root.join('db', 'seed_data', 'playoffs.rb')
