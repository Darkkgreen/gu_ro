class CreateTeamGroupTables < ActiveRecord::Migration[5.2]
  def change
    create_table :team_group_tables do |t|
      t.references :team
      t.references :group
      t.integer :points, default: 0
      t.integer :balance, default: 0

      t.timestamps
    end
  end
end
