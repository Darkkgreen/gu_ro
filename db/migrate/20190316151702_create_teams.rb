class CreateTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :teams do |t|
      t.text :name
      t.boolean :won_last_phase, default: false
      t.references :group, nil: true, default: nil
      t.references :championship, nil: false

      t.timestamps
    end
  end
end
