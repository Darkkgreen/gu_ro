class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.integer :result_team_one, default: 0
      t.integer :result_team_two, default: 0
      t.integer :playoff_level, default: 0
      t.integer :team_one_id # reference
      t.integer :team_two_id # reference
      t.references :group
      t.references :championship

      t.timestamps
    end

    add_foreign_key :games, :teams, column: :team_one_id, primary_key: :id
    add_foreign_key :games, :teams, column: :team_two_id, primary_key: :id
  end
end
