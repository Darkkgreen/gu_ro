class CreateChampionships < ActiveRecord::Migration[5.2]
  def change
    create_table :championships do |t|
      t.text :name
      t.integer :group_size, default: 5, nil: false
      t.integer :playoff_level, default: 0

      t.timestamps
    end
  end
end
