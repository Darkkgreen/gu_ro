# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_18_222703) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "championships", force: :cascade do |t|
    t.text "name"
    t.integer "group_size", default: 5
    t.integer "playoff_level", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "games", force: :cascade do |t|
    t.integer "result_team_one", default: 0
    t.integer "result_team_two", default: 0
    t.integer "playoff_level", default: 0
    t.integer "team_one_id"
    t.integer "team_two_id"
    t.bigint "group_id"
    t.bigint "championship_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["championship_id"], name: "index_games_on_championship_id"
    t.index ["group_id"], name: "index_games_on_group_id"
  end

  create_table "groups", force: :cascade do |t|
    t.integer "number"
    t.bigint "championship_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["championship_id"], name: "index_groups_on_championship_id"
  end

  create_table "team_group_tables", force: :cascade do |t|
    t.bigint "team_id"
    t.bigint "group_id"
    t.integer "points", default: 0
    t.integer "balance", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_team_group_tables_on_group_id"
    t.index ["team_id"], name: "index_team_group_tables_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.text "name"
    t.boolean "won_last_phase", default: false
    t.bigint "group_id"
    t.bigint "championship_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["championship_id"], name: "index_teams_on_championship_id"
    t.index ["group_id"], name: "index_teams_on_group_id"
  end

  add_foreign_key "games", "teams", column: "team_one_id"
  add_foreign_key "games", "teams", column: "team_two_id"
end
