# frozen_string_literal: true

Rails.logger.info 'Generating groups and results'

Championship.first.start_groups
Championship.first.simulate_group_phasing
