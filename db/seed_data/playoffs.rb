# frozen_string_literal: true

Rails.logger.info 'Generating playoffs'

Championship.first.generate_playoffs
