# frozen_string_literal: true

Faker::UniqueGenerator.clear
Rails.logger.info 'Generating Teams'

80.times do
  Team.create!(name: 'Team ' + Faker::Superhero.unique.name,
               championship_id: Championship.first.id)
end
