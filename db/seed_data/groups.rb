# frozen_string_literal: true

Rails.logger.info 'Generating Teams'

16.times do |count|
  Group.create!(championship_id: Championship.first.id, number: (count + 1))
end
